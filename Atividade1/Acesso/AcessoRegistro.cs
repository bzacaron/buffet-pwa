﻿using Buffet.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Usuario
{
    public class AcessoRegistro
    {
        private UserManager<Usuario> _userManager;
        private readonly DatabaseContext _databaseContext;
        
        private int UsuarioId;
        private DateTime Horario;

        public ICollection<AcessoRegistro> acesso { get; set; }

        public AcessoRegistro(int usuarioId, DateTime horario)
        {
            UsuarioId = usuarioId;
            this.Horario = horario;
        }

        public AcessoRegistro(DateTime registro, DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void AcessosViewModel()
        {
            acesso = new List<AcessoRegistro>();
        }

        public ICollection<AcessoRegistro> ObterTodos()
        {
            return _databaseContext.Acessos.Include(a => a).ToList();
        }

        public AcessoRegistro Cadastrar(AcessoRegistro novoRegistro)
        {
            _databaseContext.Acessos.Add(novoRegistro);
            _databaseContext.SaveChanges();
            return novoRegistro;
        }
    }
}
