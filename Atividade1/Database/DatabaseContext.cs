﻿using Buffet.Models.Buffet.Cliente;
using Buffet.Models.Buffet.Convidado;
using Buffet.Models.Buffet.Evento;
using Buffet.Models.Usuario;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Buffet.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<TipoCliente> TipoCliente { get; set; }
        public DbSet<EventoEntity> Evento { get; set; }
        public DbSet<SituacaoEvento> SituacaoEvento { get; set; }
        public DbSet<TipoEvento> TipoEventos { get; set; }
        public DbSet<ConvidadoEntity> Convidados { get; set; }
        public DbSet<AcessoRegistro> Acessos { get; set; }
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
    }
}
