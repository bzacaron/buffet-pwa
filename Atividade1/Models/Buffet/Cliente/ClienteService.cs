﻿using Buffet.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Cliente
{
    public class ClienteService
    {
        private readonly DatabaseContext _databaseContext;

        public ClienteService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<Cliente> ObterTodos()
        {
            return _databaseContext.Cliente.Include(c => c).ToList();
        }

        public Cliente Cadastrar(IDadosBasicosClienteModel dadosBasicos)
        {
            var novoCliente = ValidarDadosBasicos(dadosBasicos);
            _databaseContext.Cliente.Add(novoCliente);
            _databaseContext.SaveChanges();
            return novoCliente;
        }

        private Cliente ValidarDadosBasicos(IDadosBasicosClienteModel dadosBasicos, Cliente entidadeExistente = null)
        {
            //instanciar entidade
            var entidade = entidadeExistente ?? new Cliente();

            return entidade;
        }

        public Cliente Editar(Guid id, IDadosBasicosClienteModel dadosBasicos)
        {
            var clienteEntity = ObterPorID(id);
            clienteEntity = ValidarDadosBasicos(dadosBasicos, clienteEntity);
            _databaseContext.SaveChanges();

            return clienteEntity;
        }

        public Cliente Remover(Guid id)
        {
            var clienteEntity = ObterPorID(id);
            _databaseContext.Cliente.Remove(clienteEntity);
            _databaseContext.SaveChanges();

            return clienteEntity;
        }

        public Cliente ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.Cliente.Include(c => c).First(c => c.Id == id);
            }
            catch
            {
                throw new Exception("Clienre de ID #" + id + "não encontrado");
            }
        }
    }
    public interface IDadosBasicosClienteModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string data { get; set; }
        public string email { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }
    }
}
