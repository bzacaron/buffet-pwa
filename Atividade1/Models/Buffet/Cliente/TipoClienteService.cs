﻿using Buffet.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Cliente
{
    public class TipoClienteService
    {
        private readonly DatabaseContext _databaseContext;

        public TipoClienteService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<TipoCliente> ObterTodos()
        {
            return _databaseContext.TipoCliente.ToList();
        }

        public TipoCliente ObterPorId(int id)
        {
            try
            {
                return _databaseContext.TipoCliente.Find(id);
            } catch
            {
                throw new Exception("Tipo de cliente do ID #" + id + "não encontrado");
            }
        }
    }
}
