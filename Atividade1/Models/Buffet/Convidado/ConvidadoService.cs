﻿using Buffet.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Convidado
{
    public class ConvidadoService
    {
        private readonly DatabaseContext _databaseContext;

        public ConvidadoService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<ConvidadoEntity> ObterPorEvento()
        {
            return _databaseContext.Convidados.Include(c => c.Evento).ToList();
        }
    }
    public interface IDadosBasicosClienteModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string data { get; set; }
        public string email { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }
    }
}
