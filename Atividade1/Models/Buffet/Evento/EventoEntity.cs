﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Evento
{
    public class EventoEntity
    {
        public TipoEvento Tipo { get; set; } 
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Termino { get; set; }
        public string Local { get; set; }
        public string Endereco { get; set; }
        public DateTime Insercao { get; set; }
        public DateTime Modificacao { get; set; }
        public string Observacoes { get; set; }

        public Cliente.Cliente Cliente { get; set; }
        public SituacaoEvento Situacao { get; set; }

    }
}
