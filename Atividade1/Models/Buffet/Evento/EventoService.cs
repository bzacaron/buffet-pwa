﻿using Buffet.Database;
using Buffet.Models.Buffet.Cliente;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Evento
{
    public class EventoService
    {
        private readonly DatabaseContext _databaseContext;

        public EventoService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<EventoEntity> ObterTodos()
        {
            return _databaseContext.Evento.Include(e => e).ToList();
        }

        public EventoEntity Cadastrar(IDadosBasicosEventoModel dadosBasicos)
        {
            var novoEvento = ValidarDadosBasicos(dadosBasicos);
            _databaseContext.Evento.Add(novoEvento);
            _databaseContext.SaveChanges();
            return novoEvento;
        }

        private EventoEntity ValidarDadosBasicos(IDadosBasicosEventoModel dadosBasicos, EventoEntity entidadeExistente = null)
        {
            //instanciar entidade
            var entidade = entidadeExistente ?? new EventoEntity();

            return entidade;
        }

        public EventoEntity Editar(Guid id, IDadosBasicosEventoModel dadosBasicos)
        {
            var eventoEntity = ObterPorID(id);
            eventoEntity = ValidarDadosBasicos(dadosBasicos, eventoEntity);
            _databaseContext.SaveChanges();

            return eventoEntity;
        }

        public EventoEntity Remover(Guid id)
        {
            var eventoEntity = ObterPorID(id);
            _databaseContext.Evento.Remove(eventoEntity);
            _databaseContext.SaveChanges();

            return eventoEntity;
        }

        public EventoEntity ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.Evento.Include(e => e).First(e => e.Id == id);
            }
            catch
            {
                throw new Exception("Evento de ID #" + id + "não encontrado");
            }
        }
    }
    public interface IDadosBasicosEventoModel
    {
        public string Id { get; set; }
        public string tipo { get; set; }
        public string descricao { get; set; }
        public string inicio { get; set; }
        public string termino { get; set; }
        public string local { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }
        public string situacao { get; set; }
    }
}

