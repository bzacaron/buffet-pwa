﻿using Buffet.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Evento
{
    public class SituacaoEventoService
    {
        private readonly DatabaseContext _databaseContext;

        public SituacaoEventoService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<SituacaoEvento> ObterTodos()
        {
            return _databaseContext.SituacaoEvento.Include(s => s).ToList();
        }

        public SituacaoEvento ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.SituacaoEvento.Include(s => s).First(s => s.Id == id);
            }
            catch
            {
                throw new Exception("Situacao de Evento de ID #" + id + "não encontrado");
            }
        }
    }
}
