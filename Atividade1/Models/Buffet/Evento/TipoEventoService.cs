﻿using Buffet.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Buffet.Evento
{
    public class TipoEventoService
    {
        private readonly DatabaseContext _databaseContext;

        public TipoEventoService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<TipoEvento> ObterTodos()
        {
            return _databaseContext.TipoEventos.Include(t => t).ToList();
        }

        public TipoEvento ObterPorID(Guid id)
        {
            try
            {
                return _databaseContext.TipoEventos.Include(t => t).First(t => t.Id == id);
            }
            catch
            {
                throw new Exception("Tipo de Evento de ID #" + id + "não encontrado");
            }
        }
    }
}
