﻿using Buffet.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.Models.Usuario
{
    public class UsuarioService
    {
        private readonly DatabaseContext _databaseContext;

        public UsuarioService(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public ICollection<Usuario> ObterTodos()
        {
            return _databaseContext.Usuario.Include(u => u).ToList();
        }

        /*public List<Usuario> ObterUsuarios()
        {
            List<Usuario> usuarios = _databaseContext.Usuario.ToList();

            return usuarios;
        }*/

        public interface IDadosBasicosClienteModel
        {
            public string Id { get; set; }
            public string Login { get; set; }
            public string Senha { get; set; }
        }
    }
}
