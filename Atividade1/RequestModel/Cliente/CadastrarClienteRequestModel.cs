﻿using Buffet.Models.Buffet.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.RequestModel.Cliente
{
    public class CadastrarClienteRequestModel : IDadosBasicosClienteModel
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string cpf { get; set; }
        public string data { get; set; }
        public string email { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }
        public ICollection<string> ValidarEFiltrar()
        {
            var listaDeErros = new List<string>();
            return listaDeErros;
        }
    }
}
