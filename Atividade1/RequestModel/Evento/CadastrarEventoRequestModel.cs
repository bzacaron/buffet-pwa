﻿using Buffet.Models.Buffet.Evento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.RequestModel.Evento
{
    public class CadastrarEventoRequestModel : IDadosBasicosEventoModel
    {
        public string Id { get; set; }
        public string tipo { get; set; }
        public string descricao { get; set; }
        public string inicio { get; set; }
        public string termino { get; set; }
        public string local { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }
        public string situacao { get; set; }

        public ICollection<string> ValidarEFiltrar()
        {
            var listaDeErros = new List<string>();
            return listaDeErros;
        }
    }
}
