﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Cliente
{
    public class CadastrarClienteViewModel
    {
        public string[] FormMensagensErro { get; set; }
        public ICollection<SelectListItem> TipoCliente { get; set; }

        public CadastrarClienteViewModel()
        {
            TipoCliente = new List<SelectListItem>
            {
                new SelectListItem("Selecione", "")
            };
        }
    }
}
