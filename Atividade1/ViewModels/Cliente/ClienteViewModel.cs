﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Cliente
{
    public class ClienteViewModel
    {
        public ICollection<Clientes> clientes { get; set; }
        public string MensagemSucesso { get; set; }
        public String MensagemErro { get; set; }

        public ClienteViewModel()
        {
            clientes = new List<Clientes>();
        }

    }
     public class Clientes
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string data { get; set; }
        public string email { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }
        public string cpf { get; set; }
    }
}
