﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Cliente
{
    public class EditarClienteViewModel
    {
        public string[] FormMensagensErro { get; set; }
        public ICollection<SelectListItem> TipoCliente { get; set; }

        public string Id { get; set; }
        public string Nome { get; set; }
        public string cpf { get; set; }
        public string data { get; set; }
        public string email { get; set; }
        public string endereco { get; set; }
        public string observacao { get; set; }

        public EditarClienteViewModel()
        {
            TipoCliente = new List<SelectListItem>{
                new SelectListItem("Selecione", "")
            };
        }
    }
}
