﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Convidado
{
    public class ConvidadoViewModel
    {
        public ICollection<Convidado> convidado { get; set; }
        
        public ConvidadoViewModel()
        {
            convidado = new List<Convidado>();
        }
    }
    public class Convidado
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string CPF { get; set; }
        public string DataNascimento { get; set; }
        public string Observacoes { get; set; }
        public string Situacao { get; set; }
    }
}
