﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Evento
{
    public class CadastraEventoViewModel
    {
        public string[] FormMensagensErro { get; set; }
        public ICollection<SelectListItem> Tipo { get; set; }

        public CadastraEventoViewModel()
        {
            Tipo = new List<SelectListItem>
            {
                new SelectListItem("Selecione", "")
            };
        }
    }
}
