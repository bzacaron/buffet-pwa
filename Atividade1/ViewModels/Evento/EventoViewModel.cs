﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Evento
{
    public class EventoViewModel
    {
        public ICollection<Evento> Eventos { get; set; }
        public string MensagemSucesso { get; set; }
        public string MensagemErro { get; set; }

        public EventoViewModel()
        {
            Eventos = new List<Evento>();
        }

    }
    public class Evento
    {
        public string Id { get; set; }
        public string tipo { get; set; }
        public string situacao { get; set; }
        public string Descricao { get; set; }
        public string Inicio { get; set; }
        public string Termino { get; set; }
        public string Local { get; set; }
        public string Endereco { get; set; }
        public string Observacoes { get; set; }
    }
}
