﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Buffet.ViewModels.Evento
{
    public class RemoverEventoViewModel
    {
        public string[] FormMensagensErro { get; set; }
        public ICollection<SelectListItem> Tipo { get; set; }
        public ICollection<SelectListItem> Situacao { get; set; }
        public string Id { get; set; }
        public string Descricao { get; set; }
        public string Inicio { get; set; }
        public string Termino { get; set; }
        public string Local { get; set; }
        public string Endereco { get; set; }
        public string Modificacao { get; set; }
        public string Observacoes { get; set; }
    }
}
