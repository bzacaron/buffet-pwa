-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pwa
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `dataNascimento` date DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `endereco` varchar(120) DEFAULT NULL,
  `insercao` date DEFAULT NULL,
  `modificacao` date DEFAULT NULL,
  `observacoes` varchar(200) DEFAULT NULL,
  `idtipo` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cliente_tipo_idx` (`idtipo`),
  CONSTRAINT `fk_cliente_tipo` FOREIGN KEY (`idtipo`) REFERENCES `tipocliente` (`idtipocliente`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'teste','2021-01-21','12345678','teste@teste','endereco',NULL,NULL,'obs',NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convidado`
--

DROP TABLE IF EXISTS `convidado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `convidado` (
  `idconvidado` int NOT NULL,
  `idevento` int DEFAULT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `datanascimento` date DEFAULT NULL,
  `datainsercao` datetime DEFAULT NULL,
  `observacao` varchar(45) DEFAULT NULL,
  `idsituacao` int DEFAULT NULL,
  PRIMARY KEY (`idconvidado`),
  KEY `fk_convidado_evento_idx` (`idevento`),
  KEY `fk_convidado_situacao_idx` (`idsituacao`),
  CONSTRAINT `fk_convidado_evento` FOREIGN KEY (`idevento`) REFERENCES `evento` (`idevento`),
  CONSTRAINT `fk_convidado_situacao` FOREIGN KEY (`idsituacao`) REFERENCES `situacaoconvidado` (`idsituacaoconvidado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convidado`
--

LOCK TABLES `convidado` WRITE;
/*!40000 ALTER TABLE `convidado` DISABLE KEYS */;
/*!40000 ALTER TABLE `convidado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evento` (
  `idevento` int NOT NULL AUTO_INCREMENT,
  `idtipo` int DEFAULT NULL,
  `idcliente` int DEFAULT NULL,
  `idsituacao` int DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `termino` datetime DEFAULT NULL,
  `local` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `observacao` varchar(45) DEFAULT NULL,
  `insercao` datetime DEFAULT NULL,
  `modificacao` datetime DEFAULT NULL,
  PRIMARY KEY (`idevento`),
  KEY `fk_evento_cliente_idx` (`idcliente`),
  KEY `fk_evento_situacao_idx` (`idsituacao`),
  KEY `fk_evento_tipo_idx` (`idtipo`),
  CONSTRAINT `fk_evento_cliente` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`id`),
  CONSTRAINT `fk_evento_situacao` FOREIGN KEY (`idsituacao`) REFERENCES `situacaoevento` (`idsituacaoEvento`),
  CONSTRAINT `fk_evento_tipo` FOREIGN KEY (`idtipo`) REFERENCES `tipoevento` (`idtipoevento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historicologin`
--

DROP TABLE IF EXISTS `historicologin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `historicologin` (
  `idusuario` int DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  KEY `fk_historico_usuario_idx` (`idusuario`),
  CONSTRAINT `fk_historico_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historicologin`
--

LOCK TABLES `historicologin` WRITE;
/*!40000 ALTER TABLE `historicologin` DISABLE KEYS */;
/*!40000 ALTER TABLE `historicologin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `situacaoconvidado`
--

DROP TABLE IF EXISTS `situacaoconvidado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `situacaoconvidado` (
  `idsituacaoconvidado` int NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsituacaoconvidado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `situacaoconvidado`
--

LOCK TABLES `situacaoconvidado` WRITE;
/*!40000 ALTER TABLE `situacaoconvidado` DISABLE KEYS */;
INSERT INTO `situacaoconvidado` VALUES (1,'Confirmado'),(2,'Cancelado'),(3,'Pendente');
/*!40000 ALTER TABLE `situacaoconvidado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `situacaoevento`
--

DROP TABLE IF EXISTS `situacaoevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `situacaoevento` (
  `idsituacaoEvento` int NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsituacaoEvento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `situacaoevento`
--

LOCK TABLES `situacaoevento` WRITE;
/*!40000 ALTER TABLE `situacaoevento` DISABLE KEYS */;
INSERT INTO `situacaoevento` VALUES (1,'Agendado'),(2,'Cancelado'),(3,'Executando');
/*!40000 ALTER TABLE `situacaoevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipocliente`
--

DROP TABLE IF EXISTS `tipocliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipocliente` (
  `idtipocliente` int NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipocliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipocliente`
--

LOCK TABLES `tipocliente` WRITE;
/*!40000 ALTER TABLE `tipocliente` DISABLE KEYS */;
INSERT INTO `tipocliente` VALUES (1,'Pessoa Fisica'),(2,'Pessoa Juridica');
/*!40000 ALTER TABLE `tipocliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoevento`
--

DROP TABLE IF EXISTS `tipoevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoevento` (
  `idtipoevento` int NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoevento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoevento`
--

LOCK TABLES `tipoevento` WRITE;
/*!40000 ALTER TABLE `tipoevento` DISABLE KEYS */;
INSERT INTO `tipoevento` VALUES (1,'Aniversário'),(2,'Casamento'),(3,'Evento Social'),(4,'Outro');
/*!40000 ALTER TABLE `tipoevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `idusuario` int NOT NULL AUTO_INCREMENT,
  `login` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'luciano','123'),(2,'adm','123'),(3,'gabriela','159745'),(4,'miguel','peixeforadagua');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'pwa'
--

--
-- Dumping routines for database 'pwa'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-19 19:35:02
